
# Hyper Launch Menu

Adds ability to launch menus to Hyper

![Screenshot](screenshot.png)

## Installation

Run

`hyper i hyper-launch-menu`

## Configuration

Add the `otherShells` key to your `hyper.js` with a list of shells, e.g.:

```
// List of other shells to add to launch menu
 otherShells: [
   {shell: "C:\\Windows\\System32\\cmd.exe", shellArgs: [], shellName: "CMD"},
   {shell: "C:\\Program Files\\Git\\bin\\bash.exe", shellArgs: ["--login"], shellName: "Git Bash"},
   {shell: "powershell.exe", shellArgs: [], shellName: "Powershell"},
   {shell: "C:\\cygwin64\\bin\\bash.exe", shellArgs: ["--login"], shellName: "Cygwin"},
   {shell: "cmd.exe", shellName: "Visual Studio", shellArgs: ["/k", "C:\\Program Files (x86)\\Microsoft Visual Studio 14.0\\VC\\vcvarsall.bat", "amd64"]}
 ],
```
